From e29f88ce772ebceccee500d2c05733ee0336849f Mon Sep 17 00:00:00 2001
From: Giulio Camuffo <giulio.camuffo@kdab.com>
Date: Mon, 16 Mar 2020 10:22:36 +0100
Subject: Move the wayland socket polling to a separate event thread

A new event thread is introduced which calls poll() on the wayland fd,
instead of relying on the event dispatcher by using the QSocketNotifier.
This allows to call in the proper order the wl_display_prepare_read(),
poll() and wl_display_read_events() functions.

[ratchanan@ubports.com: modified to work on Qt 5.12.
  - QAtomicInt::loadRelaxed() -> QAtomicInt::load()
  - Remove the test; it uses the new test shared code.
This patch is added for accelerated viewfinder on PinePhone.]

Co-authored-by: Ratchanan Srirattanamet <ratchanan@ubports.com>
Task-number: QTBUG-66075
Change-Id: Ibb33ad7f4193b866d1b8d7a0405a94d59dcad5eb
Bug: https://bugreports.qt.io/browse/QTBUG-66075
Bug-UBports: https://gitlab.com/ubports/community-ports/pinephone/-/issues/37
Origin: https://codereview.qt-project.org/c/qt/qtwayland/+/301712
Forwarded: https://codereview.qt-project.org/c/qt/qtwayland/+/301712
Last-Update: 2020-09-10
---

--- a/src/client/qwaylanddisplay.cpp
+++ b/src/client/qwaylanddisplay.cpp
@@ -81,6 +81,122 @@
 
 namespace QtWaylandClient {
 
+class EventThread : public QThread
+{
+public:
+    EventThread(wl_display *wl, QWaylandDisplay *disp)
+        : m_fd(wl_display_get_fd(wl))
+        , m_pipefd{ -1, -1 }
+        , m_wldisplay(wl)
+        , m_display(disp)
+        , m_reading(true)
+        , m_quitting(false)
+    {
+        setObjectName(QStringLiteral("WaylandEventThread"));
+    }
+
+    void stop()
+    {
+        Q_ASSERT(QThread::currentThread() == m_display->thread());
+
+        // We have to both write to the pipe and set the flag, as the thread may be
+        // either in the poll() or waiting for _prepare_read().
+        if (m_pipefd[1] != -1 && write(m_pipefd[1], "\0", 1) == -1)
+            qWarning("Failed to write to the pipe: %s.", strerror(errno));
+
+        {
+            QMutexLocker l(&m_mutex);
+            m_quitting = true;
+            m_cond.wakeOne();
+        }
+
+        wait();
+    }
+
+protected:
+    void run() override
+    {
+        // we use this pipe to make the loop exit otherwise if we simply used a flag on the loop condition, if stop() gets
+        // called while poll() is blocking the thread will never quit since there are no wayland messages coming anymore.
+        struct Pipe
+        {
+            Pipe(int *fds)
+                : fds(fds)
+            {
+                if (qt_safe_pipe(fds) != 0)
+                    qWarning("Pipe creation failed. Quitting may hang.");
+            }
+            ~Pipe()
+            {
+                if (fds[0] != -1) {
+                    close(fds[0]);
+                    close(fds[1]);
+                }
+            }
+
+            int *fds;
+        } pipe(m_pipefd);
+
+        // Make the main thread call wl_prepare_read(), dispatch the pending messages and flush the
+        // outbound ones. Wait until it's done before proceeding, unless we're told to quit.
+        while (waitForReading()) {
+            pollfd fds[2] = { { m_fd, POLLIN, 0 }, { m_pipefd[0], POLLIN, 0 } };
+            poll(fds, 2, -1);
+
+            if (fds[1].revents & POLLIN) {
+                // we don't really care to read the byte that was written here since we're closing down
+                wl_display_cancel_read(m_wldisplay);
+                break;
+            }
+
+            if (fds[0].revents & POLLIN)
+                wl_display_read_events(m_wldisplay);
+                // The polll was succesfull and the event thread did the wl_display_read_events(). On the next iteration of the loop
+                // the event sent to the main thread will cause it to dispatch the messages just read, unless the loop exits in which
+                // case we don't care anymore about them.
+            else
+                wl_display_cancel_read(m_wldisplay);
+        }
+    }
+
+private:
+    bool waitForReading()
+    {
+        Q_ASSERT(QThread::currentThread() == this);
+
+        m_reading.storeRelease(false);
+        QMetaObject::invokeMethod(m_display, &QWaylandDisplay::flushRequests, Qt::QueuedConnection);
+
+        QMutexLocker lock(&m_mutex);
+        // m_reading might be set from our invoke or some other invocation of flushRequests().
+        while (!m_reading.load() && !m_quitting)
+            m_cond.wait(&m_mutex);
+
+        return !m_quitting;
+    }
+
+    int m_fd;
+    int m_pipefd[2];
+    wl_display *m_wldisplay;
+    QWaylandDisplay *m_display;
+
+    /* Concurrency note:
+     * m_reading is set to false inside event thread's waitForReading(), and is
+     * set to true inside main thread's flushRequests().
+     * The lock is not taken when setting m_reading to false, as the main thread
+     * is not actively waiting for it to turn false. However, the lock is taken
+     * inside flushRequests() before setting m_reading to true,
+     * as the event thread is actively waiting for it under the wait condition.
+     */
+
+    QAtomicInteger<bool> m_reading;
+    bool m_quitting;
+    QMutex m_mutex;
+    QWaitCondition m_cond;
+
+    friend class QWaylandDisplay;
+};
+
 Q_LOGGING_CATEGORY(lcQpaWayland, "qt.qpa.wayland"); // for general (uncategorized) Wayland platform logging
 
 struct wl_surface *QWaylandDisplay::createSurface(void *handle)
@@ -150,6 +266,9 @@
 
 QWaylandDisplay::~QWaylandDisplay(void)
 {
+    if (m_eventThread)
+        m_eventThread->stop();
+
     if (mSyncCallback)
         wl_callback_destroy(mSyncCallback);
 
@@ -184,14 +303,42 @@
 
 void QWaylandDisplay::flushRequests()
 {
-    if (wl_display_prepare_read(mDisplay) == 0) {
-        wl_display_read_events(mDisplay);
+    /*
+     * Dispatch pending events and flush the requests at least once. If the event thread
+     * is not reading, try to call _prepare_read() to allow the event thread to poll().
+     * If that fails, re-try dispatch & flush again until _prepare_read() is successful.
+     *
+     * This allow any call to flushRequests() to start event thread's polling, not only
+     * the one issued from event thread's waitForReading(), which means functions called
+     * from despatch_pending() can safely spin an event loop.
+     */
+    for (;;) {
+        if (wl_display_dispatch_pending(mDisplay) < 0)
+            checkError();
+
+        wl_display_flush(mDisplay);
+
+        // We have to check if event thread is reading every time we dispatch
+        // something, as that may recursively call this function.
+        if (!m_eventThread || m_eventThread->m_reading.loadAcquire())
+            break;
+
+        if (wl_display_prepare_read(mDisplay) == 0) {
+            QMutexLocker l(&m_eventThread->m_mutex);
+            m_eventThread->m_reading.storeRelease(true);
+            m_eventThread->m_cond.wakeOne();
+            break;
+        }
     }
+}
 
-    if (wl_display_dispatch_pending(mDisplay) < 0)
-        checkError();
-
-    wl_display_flush(mDisplay);
+// We have to wait until we have an eventDispatcher before creating the eventThread,
+// otherwise forceRoundTrip() may block inside _events_read() because eventThread is
+// polling.
+void QWaylandDisplay::initEventThread()
+{
+    m_eventThread.reset(new EventThread(mDisplay, this));
+    m_eventThread->start();
 }
 
 void QWaylandDisplay::blockingReadEvents()
--- a/src/client/qwaylanddisplay_p.h
+++ b/src/client/qwaylanddisplay_p.h
@@ -96,6 +96,7 @@
 class QWaylandHardwareIntegration;
 class QWaylandShellIntegration;
 class QWaylandCursorTheme;
+class EventThread;
 
 typedef void (*RegistryListener)(void *data,
                                  struct wl_registry *registry,
@@ -185,6 +186,7 @@
     wl_event_queue *createEventQueue();
     void dispatchQueueWhile(wl_event_queue *queue, std::function<bool()> condition, int timeout = -1);
 
+    void initEventThread();
 public slots:
     void blockingReadEvents();
     void flushRequests();
@@ -207,6 +209,7 @@
     };
 
     struct wl_display *mDisplay = nullptr;
+    QScopedPointer<EventThread> m_eventThread;
     QtWayland::wl_compositor mCompositor;
     QScopedPointer<QWaylandShm> mShm;
     QList<QWaylandScreen *> mScreens;
--- a/src/client/qwaylandintegration.cpp
+++ b/src/client/qwaylandintegration.cpp
@@ -224,10 +224,7 @@
     QAbstractEventDispatcher *dispatcher = QGuiApplicationPrivate::eventDispatcher;
     QObject::connect(dispatcher, SIGNAL(aboutToBlock()), mDisplay.data(), SLOT(flushRequests()));
     QObject::connect(dispatcher, SIGNAL(awake()), mDisplay.data(), SLOT(flushRequests()));
-
-    int fd = wl_display_get_fd(mDisplay->wl_display());
-    QSocketNotifier *sn = new QSocketNotifier(fd, QSocketNotifier::Read, mDisplay.data());
-    QObject::connect(sn, SIGNAL(activated(int)), mDisplay.data(), SLOT(flushRequests()));
+    mDisplay->initEventThread();
 
     if (mDisplay->screens().isEmpty()) {
         qWarning() << "Running on a compositor with no screens is not supported";
